const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./dbConfig')
const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get('/rubrica/list', async (req, res) => {
    try {
        let elenco = await pool.query(`SELECT * FROM contatto`);
        res.json(elenco)
    } catch (error) {
        console.log(error);
    }
})


app.get('/rubrica/list/:id', async (req, res) => {
    try {
        let contatto = await pool.query(`SELECT * FROM contatto WHERE id=${req.params.id}`);
        res.json(contatto.rows)
    } catch (error) {
        console.log(error);
    }
})

app.post('/rubrica/agg', async (req, res) => {
    try {
        let queryStr = `INSERT INTO contatto (nome,cognome,phone,email,gender)` +
            `VALUES ('${req.body.nome}','${req.body.cognome}','${req.body.phone}','${req.body.email}','${req.body.gender}')`
        let ins = await pool.query(queryStr)
        if (ins.rowCount > 0) {
            res.json({ status: "success" })
        }
        else {
            res.json({ status: "error" })
        }
    } catch (error) {
        console.log(error);
    }
})

app.delete('/rubrica/canc/:id', async (req, res) => {
    try {
        let del = await pool.query(`DELETE FROM contatto WHERE id=${req.params.id}`)
        if (del.rowCount > 0) {
            res.json({ status: "success" })
        }
        else {
            res.json({ status: "error" })
        }
    } catch (error) {
        console.log(error);
    }
})

app.put('/rubrica/update/:id', async (req, res) => {
    try {
        let queryPreSave = await pool.query(`SELECT * FROM contatto WHERE id=${req.params.id}`)
        let contattoPreSave = queryPreSave.rows[0]

        if (req.body.nome) {
            contattoPreSave.nome = req.body.nome
        }
        if (req.body.cognome) {
            contattoPreSave.cognome = req.body.cognome
        }
        if (req.body.phone) {
            contattoPreSave.phone = req.body.phone
        }
        if (req.body.email) {
            contattoPreSave.email = req.body.email
        }
        if (req.body.gender) {
            contattoPreSave.gender = req.body.gender
        }

        let queryStr = `UPDATE contatto SET nome='${contattoPreSave.nome}',` +
            `cognome='${contattoPreSave.cognome}',phone='${contattoPreSave.phone}',` +
            `email='${contattoPreSave.email}',gender='${contattoPreSave.gender}'` +
            `WHERE id=${req.params.id}`
        let update = await pool.query(queryStr)
        if (update.rowCount > 0) {
            res.json({ status: "success" })
        }
        else {
            res.json({ status: "error" })
        }

    } catch (error) {
        console.log(error);
    }
})