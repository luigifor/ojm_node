const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./dbConfig')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extender: true }))

const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/piatti/list", async (req, res) => {
    try {
        let elenco = await pool.query(`SELECT * FROM piatti`);
        res.json(elenco.rows)
    } catch (error) {
        console.log(error);
    }
})

app.get("/piatti/:idPiatto", async (req, res) => {
    try {
        let risultato = await pool.query(`SELECT * FROM piatti WHERE id=${req.params.idPiatto}`)
        res.json(risultato.rows)
    } catch (error) {
        console.log(error);
    }
})

app.post("/piatti/insert", async (req, res) => {
    console.log("inizio");
    try {
        let querySql = `INSERT INTO piatti (nome, descrizione, prezzo, glutine)` +
            `values ('${req.body.nome}', '${req.body.descrizione}', ${req.body.prezzo}, ${req.body.glutine})`;

        let inserimento = await pool.query(querySql);

        if (inserimento.rowCount > 0) {
            res.json({ status: "success" })
        }
        else {
            res.json({ status: "error" })
        }

    } catch (error) {
        console.log(error);
    }
})

app.delete("/piatti/:idPiatto", async (req, res) => {
    try {
        let risultato = await pool.query(`DELETE FROM piatti WHERE id=${req.params.idPiatto}`)

        if (risultato.rowCount > 0) {
            res.json({ status: "eliminato con successo!" })
        }
        else {
            res.json({ status: "errore, dato non trovato" })
        }
    } catch (error) {
        console.log(error);
    }
})

app.put("/piatti/:idPiatto", async (req, res) => {
    try {
        let risultato = await pool.query(`UPDATE piatti SET nome='${req.body.nome}', descrizione='${req.body.descrizione}',prezzo='${req.body.prezzo}',glutine='${req.body.glutine}' WHERE id=${req.params.idPiatto}`)

        if (risultato.rowCount > 0) {
            res.json({ status: "aggiornato con successo!" })
        }
        else {
            res.json({ status: "errore, dato non trovato" })
        }
    } catch (error) {
        console.log(error);
    }
})

