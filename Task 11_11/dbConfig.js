const pg = require('pg')
const Pool = pg.Pool

const pool = new Pool({
    user: "admin",
    password: "admin",
    database: "ristorante",
    host: "localhost",
    port: 5432
})

module.exports = pool