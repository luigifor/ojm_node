const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressSession = require('express-session')
const fileUpload = require('express-fileupload')

const app = express()

app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(fileUpload())

app.use(expressSession({
    secret: "boh",
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: 'auto',
        maxAge: 3600000
    }
}))

const port = 4000


// CONNESSIONE AL DB
const db = mongoose.connect('mongodb+srv://user1:user1@cluster0.mzmrq.mongodb.net/GestioneSubs?retryWrites=true&w=majority', () => {
    console.log('Sono connesso al DB')
})

// AVVIO SERVER
app.listen(port, () => {
    console.log('Server connesso')
})

global.loggato = null
global.ruolo = null

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    ruolo = req.session.role

    console.log(loggato, ruolo)
    next()
})
// MIDDLEWARES
const checkLogin = require('./middlewares/checkLogin')
const checkAdminMiddleware = require('./middlewares/checkAdminMiddleware')

//ROTTE
const homeController = require('./controllers/homeController')
const registerPageController = require('./controllers/registerPageController')
const registerActionController = require('./controllers/registerActionController')
const loginPageController = require('./controllers/loginPageController')
const loginActionController = require('./controllers/loginActionController')
const logoutController = require('./controllers/logoutController')
const orderPageController = require('./controllers/orderPageController')
const orderActionController = require('./controllers/orderActionController')
const inserimentoPageController = require('./controllers/inserimentoPageController')
const inserimentoActionController = require('./controllers/inserimentoActionController')
const elencoSubsPageController = require('./controllers/elencoSubsPageController')
const ordersLogPageController = require('./controllers/ordersLogPageController')
const elencoUtentiPageController = require('./controllers/elencoUtentiPageController')
const orderDeleteActionController = require('./controllers/orderDeleteActionController')
const userDeleteActionController = require('./controllers/userDeleteActionController')
const subDeleteActionController = require('./controllers/subDeleteActionController')
const subModifyActionController = require('./controllers/subModifyActionController')
const subModifyPageController = require('./controllers/subModifyPageController')





app.get('/', homeController)
// AUTENTIFICAZIONE
app.get('/auth/register', registerPageController)
app.post('/auth/register', registerActionController)
app.get('/auth/login', loginPageController)
app.post('/auth/login', loginActionController)
app.get('/auth/logout', logoutController)
// ORDINI
app.get('/order', checkLogin, orderPageController)
app.post('/order', checkLogin, orderActionController)
// INSERIMENTO SERVIZI
app.get('/insub', checkAdminMiddleware, inserimentoPageController)
app.post('/insub', checkAdminMiddleware, inserimentoActionController)
// VISUALIZZA ELENCO SERVIZI DISPONIBILI
app.get('/elencosubs', elencoSubsPageController)
// VISUALIZZA RESOCONTO ORDINI ADMIN E USER
app.get('/orderslog', checkLogin, ordersLogPageController)
// MODIFICA E ELIMINA SERVIZI DISPONIBILI
app.get('/eliminasub/:id', checkAdminMiddleware, subDeleteActionController)
app.get('/modificasub/:id', checkAdminMiddleware, subModifyPageController)
app.post('/modificasub/:id', checkAdminMiddleware, subModifyActionController)

// VISUALIZZA LISTA UTENTI
app.get('/elencoutenti', checkAdminMiddleware, elencoUtentiPageController)
// ELIMINA UTENTI
app.get('/eliminautente/:id', checkAdminMiddleware, userDeleteActionController)
// ELIMINA ORDINE
app.get('/eliminaordine/:id', checkAdminMiddleware, orderDeleteActionController)





//REFACTOR GENERALE E VALIDAZIONE