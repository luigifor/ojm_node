const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrderSchema = new Schema(
    {
        nome: String,
        desc: String,
        totCost: Number,
        durn: Number,
        date: Date,
        user: String,
        username: String,
        paym: String
    }
)

const Order = mongoose.model('Order', OrderSchema)
module.exports = Order