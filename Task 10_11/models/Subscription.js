const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SubscriptionSchema = new Schema(
    {
        name: String,
        desc: String,
        cost: Number,
        image: String
    }
)

const Subscription = mongoose.model('Subscription', SubscriptionSchema)
module.exports = Subscription