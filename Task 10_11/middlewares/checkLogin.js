module.exports = (req, res, next) => {
    if (req.session.userId) {
        next();
    }
    else {
        res.render('error', {
            details: "Non sei loggato"
        })
    }
}