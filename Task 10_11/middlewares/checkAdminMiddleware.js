module.exports = (req, res, next) => {
    if (req.session.userId && req.session.role == "ADMIN") {
        next();
    }
    else {
        res.render('error', {
            details: "Non sei autorizzato"
        })
    }
}