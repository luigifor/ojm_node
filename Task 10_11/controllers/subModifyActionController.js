const Subscription = require('../models/Subscription')

module.exports = (req, res) => {
    let newSub = {
        name: req.body.inputName,
        desc: req.body.inputDesc,
        cost: req.body.inputCost
    }
    Subscription.findByIdAndUpdate(req.params.id, newSub, (err, sub) => {
        if (!err) {
            res.redirect('/elencosubs')
        }
        else
            res.render('error', {
                details: err._message
            })
    })
}