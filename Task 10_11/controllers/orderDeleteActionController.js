
const Order = require('../models/Order')

module.exports = (req, res) => {
    Order.findByIdAndDelete(req.params.id, (err) => {
        if (!err) {
            res.redirect('/orderslog')
        } else {
            res.render('error', {
                details: err._message
            })
        }
    })
}