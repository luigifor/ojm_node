const Subscription = require('../models/Subscription')

module.exports = async (req, res) => {
    try {
        let sub = await Subscription.findOne({ _id: req.params.id })
        res.render('mod_sub', {
            sub: sub
        })
    } catch (err) {
        res.render('error',
            {
                detail: err._message
            })
    }
}