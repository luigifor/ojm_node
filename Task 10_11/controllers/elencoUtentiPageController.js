const User = require('../models/User')

module.exports = async (req, res) => {
    try {
        let elenco = await User.find({})
        res.render('elenco_utenti', {
            elenco: elenco
        })
    } catch (err) {
        res.render('error',
            {
                detail: err._message
            })
    }

}