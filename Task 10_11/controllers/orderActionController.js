const Order = require('../models/Order')
const Subscription = require('../models/Subscription')
const User = require('../models/User')

module.exports = async (req, res) => {

    let utente = await User.findById(req.session.userId)
    let ordine = {
        nome: req.body.inputNome,
        durn: req.body.inputDurn,
        desc: "",
        totCost: 0,
        paym: req.body.inputPaym,
        date: new Date(),
        user: req.session.userId,
        username: utente.username
    }
    try {
        Subscription.findOne({
            name: ordine.nome
        }, (err, prodFound) => {
            if (!err && prodFound) {
                ordine.desc = prodFound.desc
                ordine.totCost = prodFound.cost * ordine.durn
                Order.create(ordine, (err, ordCreated) => {
                    if (!err && ordCreated) {
                        res.redirect('/orderslog')
                    } else {
                        res.render('error', {
                            details: err._message
                        })
                    }
                })
            } else {
                res.render('error', {
                    details: err._message
                })
            }
        })
    } catch (err) {
        res.render('error', {
            details: err._message
        })
    }
}