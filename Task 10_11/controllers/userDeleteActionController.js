const User = require('../models/User')

module.exports = (req, res) => {
    User.findByIdAndDelete(req.params.id, (err) => {
        if (!err) {
            res.redirect('/elencoutenti')
        } else {
            res.render('error', {
                details: err._message
            })
        }
    })
}