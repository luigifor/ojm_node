const Subscription = require('../models/Subscription')
const path = require('path')

module.exports = (req, res) => {

    let img = req.files.inputFile
    if (img.size > 100000) {
        res.end("Immagine troppo grande")
    }
    img.mv(path.resolve(__dirname, "..", "public/img", img.name), (err) => {
        if (!err) {
            let sub = {
                name: req.body.inputName,
                desc: req.body.inputDesc,
                cost: req.body.inputCost,
                image: img.name
            }
            Subscription.create(sub, (err, subCreated) => {
                if (!err && subCreated) {
                    res.redirect('/elencosubs')
                } else {
                    res.render('error', {
                        detail: err._message
                    })
                }
            })

        } else {
            res.render('error', {
                details: err._message
            })
        }
    })





}