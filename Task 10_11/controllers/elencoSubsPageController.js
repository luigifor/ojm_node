const Subscription = require('../models/Subscription')

module.exports = async (req, res) => {
    try {
        let elenco = await Subscription.find({})
        res.render('elenco_subs', {
            elenco: elenco
        })
    } catch (err) {
        res.render('error',
            {
                detail: err._message
            })
    }

}