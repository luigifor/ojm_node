const Subscription = require('../models/Subscription')
const Order = require('../models/Order')


module.exports = async (req, res) => {
    let search;

    if (ruolo == "ADMIN") {
        search = {}

    } else {
        search = { user: req.session.userId }

    }
    try {

        let elenco = await Order.find(search)
        res.render('orders_log', {
            elenco: elenco
        })
    } catch (err) {
        res.render('error',
            {
                detail: err._message
            })
    }

}