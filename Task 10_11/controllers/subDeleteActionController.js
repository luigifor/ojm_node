const Subscription = require('../models/Subscription')

module.exports = (req, res) => {
    Subscription.findByIdAndDelete(req.params.id, (err) => {
        if (!err) {
            res.redirect('/elencosubs')
        } else {
            res.render('error', {
                details: err._message
            })
        }
    })
}